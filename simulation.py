import numpy as np


# Car entity class
class Car:
    def __init__(self, num, route, streetDict):
        self.num = num # The unique ID of the car
        self.inters = routeToInters(route, streetDict) # List of intersections to travel along
        self.remaining = 0 # Remaining steps until intersection (always starts at intersection)
        self.comingFrom = streetDict[route[0]][0] # ID of the last intersection to cross
        self.arrived = -1 # -1 if not arrived. Time step otherwise
        self.waiting = False # Whether it is waiting at an intersection (sets to true immediately)

    # Getters
    def getID(self):
        return self.num

    def getRoute(self):
        return self.inters

    def getArrived(self):
        return self.arrived

    # Returns the ID of the intersection the car is going to
    def getPos(self):
        return self.inters[0]

    
    # Update the state/position of the cart
    def updateCar(self, cityGraph, interList, timeStep):
        if self.arrived == -1: # Only update if not arrived
            if self.remaining > 0: # If not waiting, move forward
                self.remaining -= 1

            if self.remaining == 0:
                if len(self.inters) <= 1: # If car has just arrived at destination
                    self.arrived = timeStep
                else: # Otherwise is waiting on a stoplight
                    if not self.waiting: # Only append if just arrived
                        interList[self.getPos()].appendToQueue(self.getID()) # Add it to the queue
                        self.waiting = True

                    if interList[self.getPos()].getState() == self.comingFrom: # If green light
                        if interList[self.getPos()].getQueue()[0] == self.getID(): # If first in queue
                            self.comingFrom = self.getPos() # Current intersection is now crossed
                            self.inters = self.inters[1:] # Removed crossed intersection from route
                            self.remaining = int(cityGraph[self.comingFrom, self.inters[0]]-1)
                            interList[self.comingFrom].popQueue() # Remove it from the intersection queue
                            self.waiting = False
                    # Else keep waiting



# Intersection entity class
class Inter:
    def __init__(self, num, streets, schedule):
        self.num = num # The unique ID of the intersection
        self.incoming = streets # Numpy array of IDs of inters at the beginning all incomming streets
        self.stateList, self.timesList = zip(*schedule[self.num]) # States to switch between and how much time to spend on each

        if schedule == []:
            self.indexState = -1 # -1 for all closed; index of state otherwise
        else:
            self.indexState = 0 # Start from the beginning

        self.queue = [] # List of cars waiting at the intersection
        self.remainingTime = schedule[self.num][0][1] # Time remaining to switch state


    # Getters
    def getState(self):
        return self.stateList[self.indexState]

    def getIncoming(self):
        return self.incoming

    def getQueue(self):
        return self.queue


    # Add a new car to the end of the queue
    def appendToQueue(self, carID):
        self.queue.append(carID)


    # Remove the first car from the queue
    def popQueue(self):
        if len(self.queue) == 1:
            self.queue = []
        else:
            self.queue = self.queue[1:]


    # Update the state according to schedule
    def updateState(self, schedule, timeStep):
        if self.remainingTime == 0:
            # Change state
            self.indexState = (self.indexState + 1) % len(self.stateList)
            remainingTime = schedule[self.num][self.indexState][1]
        else:
            self.remainingTime -= 1



# Transform a list of streets to a list of intersection IDs
def routeToInters(route, streetDict):
    return [streetDict[street][1] for street in route]


# Read the input and setup some structures
def readData():
    f = open("input.txt", "r")
    lines= f.readlines()

    # Parse general variables from the input
    timeLimit, numInters, numStreets, numCars, bonus = [int(i) for i in lines[0].strip().split(' ')]

    streetDict = {} # Allows to obtain start and end intersections from the name of the street
    cityGraph = np.zeros((numInters,numInters)) # Graph matrix where entry ij is the length of the street from intersection i to j. It need not be symmetric and 0 means there is no path

    # Index every street into citygraph and the dictionary
    for s in range(numStreets):
        start, finish, name, length = lines[s+1].strip().split(' ')
        streetDict[name] = (int(start), int(finish))

        cityGraph[int(start),int(finish)] = int(length)

    # Generate all the car entities
    carList = []
    for c in range(numCars):
        carInput = lines[numStreets+c+1].strip().split(' ')
        carList.append(Car(c, carInput[1:], streetDict))
    
    return timeLimit, numInters, numStreets, numCars, bonus, streetDict, cityGraph, carList


# Generate the intersection entities
def setupSim(numInters, cityGraph, schedule):
    interList = []
    for i in range(numInters):
        # Create intersection
        streets = np.nonzero(cityGraph[:,i])[0]
        interList.append(Inter(i, streets, schedule))

    return interList


def updateCars(cityGraph, carList, interList, timeStep):
    for c in carList:
        c.updateCar(cityGraph, interList, timeStep)


def updateInters(interList, schedule, timeStep):
    for i in interList:
        i.updateState(schedule, timeStep)


# Perform a single step (update) of the simulation entities
def executeSimStep(cityGraph, carList, interList, schedule, timeStep):
    updateCars(cityGraph, carList, interList, timeStep)
    updateInters(interList, schedule, timeStep)


# Returns the score the given schedule would obtain
def countPoints(bonus, carList, timeLimit):
    points = 0
    for c in carList:
        aC = c.getArrived()
        if aC != -1:
            points += bonus             # Bonus for each car that has arrived
            points += (timeLimit-aC-1)  # Points for arriving early

    return points


def runFullSim(schedule):
    timeLimit, numInters, numStreets, numCars, bonus, streetDict, cityGraph, carList = readData()

    interList = setupSim(numInters, cityGraph, schedule)

    for timeStep in range(timeLimit):
        executeSimStep(cityGraph, carList, interList, schedule, timeStep)

    return countPoints(bonus, carList, timeLimit)


# Example schedule and how to call the function
# Note that it does not allow for intersections with an empty schedule

print(runFullSim([[(2,2)],[(3,2),(0,1)],[(1,3)], [(2,3)]]))
