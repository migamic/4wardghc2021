# Google HashCode 2021 - Team 4ward

The original statement and the data sets can be found at the [official site](https://hashcode.withgoogle.com).

## Team 4ward
* Jaume Ros Alonso
* Miquel Martínez De Morentin Cardoner
* Verónica Rina García
* Emili Bonet Cervera

## Structure
* `optimizer.py` (Miquel Martínez De Morentin Cardoner & Emili Bonet Cervera) is responsible for generating multiple candidate schedules and try to come up with the optimal one. It relies on `simulation.py` to obtain the score that a given schedule would produce.

* `simulation.py` (Jaume Ros Alonso & Verónica Rina García) simulates from start to finish the behaviour of the vehicles according to a given schedule. It is meant to be run independently from `optimizer.py` (i.e. reads and processes the input by itself), only requiring the schedule to be passed as a parameter.

* `optimizerpD.py` (Miquel Martínez De Morentin Cardoner) is an alternative optimizer built around data set D. It tries to obtain any score > 0 rather than finding an optimal one. It does not run the simulation.

## Execution
`$ python optimizer.py`

It assumes that both code files are in the same directory, as well as the input data, which should be called `input.txt` and follow the appropriate format.

## Score

We obtained a score of **6,616,124** during the extended round. The distribution of points was as follows:

Data set | Score
--- | ---
A – _An example_ | 1,001
B – _By the ocean_ | 4,550,608
C – _Checkmate_ | 1,040,669
D – _Daily commute_ | 4,600
E – _Etoile_ | 660,293
F – _Forever jammed_ | 358,953
