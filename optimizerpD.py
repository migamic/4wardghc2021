def readData():
    f = open("d.txt", "r")
    lines= f.readlines()

    # Parse general variables from the input
    timeLimit, numInters, numStreets, numCars, bonus = [int(i) for i in lines[0].strip().split(' ')]

    streetDict = {} # Allows to obtain start and end intersections from the name of the street

    # Index every street into citygraph and the dictionary
    for s in range(numStreets):
        start, finish, name, length = lines[s+1].strip().split(' ')
        streetDict[name] = (length, finish)

    # Generate all the car entities
    carList = []
    for c in range(numCars):
        carInput = lines[numStreets+c+1].strip().split(' ')
        carList.append(carInput[1:])
    f.close()
    return timeLimit, numInters, numStreets, numCars, bonus, streetDict, carList


def compute(car, streetDict):
    time=0
    for street in car:
        time+=int(streetDict[street][0])
    return time


def write_scheduler(bestcar, streetDict):
    f = open("output.txt", "w")
    f.write("200\n")
    for street in bestcar:
        f.write(streetDict[street][1] + "\n")
        f.write("1\n")
        f.write(street+" 1\n")
    f.close()
        

timeLimit, numInters, numStreets, numCars, bonus, streetDict, carList = readData()
besttime=2*timeLimit

for car in carList:
    time=compute(car, streetDict)
    if time<besttime: 
        besttime=time
        bestcar=car
write_scheduler(bestcar, streetDict)
