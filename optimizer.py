import numpy as np
import random as rd
from math import exp
#rd.seed(123)

import simulation as si

# Params metaheurística
temp  = 1e5
alpha = 1-1e-5

# Params problema
timeLimit = None
#numInters = None
#numStreets = None
#numCars = None
#bonus = None

# Estructures
#cityGraph = None
#streetDict = {}
#id2name = {}
inter = None


def choosetochange(scheduler):
    intertochange=[]
    for i in range(10):
        ran = rd.randrange(len(scheduler))
        if not ran in intertochange:
            intertochange.append(ran)
    return intertochange


def choosenew(scheduler, intertochange):
    newscheduler=scheduler[:]
    for i in intertochange:
        interlen = len(newscheduler[i])
        if interlen<=1:
            continue
        if rd.random() < .5:    
            p1 = rd.randrange(interlen)
            p2 = rd.randrange(interlen)
            newscheduler[i][p1], newscheduler[i][p2] = newscheduler[i][p2] , newscheduler[i][p1]
        else:
            pos = rd.randrange(interlen)
            if newscheduler[i][pos][1]==0: 
                newscheduler[i][pos][1]=4
            elif rd.random() < .5: 
                newscheduler[i][pos][1]=newscheduler[i][pos][1]+2
                if newscheduler[i][pos][1]>timeLimit: newscheduler[i][pos][1]=timeLimit
            else: 
                newscheduler[i][pos][1]=newscheduler[i][pos][1]-2
    return newscheduler
    
def choosecommit(points, newpoints, temp):
    prob=exp((points-newpoints)/-temp)
    if prob>rd.random():
        return True
    return False


def schedule(scheduler, temp, points):
    intertochange=choosetochange(scheduler)
    newscheduler=choosenew(scheduler, intertochange)
    newpoints = si.runFullSim(scheduler)
    best=False    
    if newpoints>points:
        points=newpoints
        best=True
        scheduler=newscheduler
    elif choosecommit(points, newpoints, temp):
        scheduler=newscheduler

    return scheduler, points, best


def readCityGraph():
    f = open("input.txt", "r")
    lines= f.readlines()
    
    global timeLimit
    timeLimit, numInters, numStreets, numCars, bonus = [int(i) for i in lines[0].split(' ')]

    cityGraph = [[None for i in range(numInters)] for j in range(numInters)]

    for s in range(numStreets):
        start, finish, name, length = lines[s+1].split(' ')
        cityGraph[int(finish)][int(start)] = name

    return cityGraph, numInters


def init_scheduler():
    scheduler = []

    cityGraph, numInters = readCityGraph()
    for i in range(numInters):
        intersched = []
        for j,name in enumerate(cityGraph[i]):
            if name:
                intersched.append([j,rd.randrange(1,4)*2])
        scheduler.append(intersched)
    return scheduler, cityGraph

def write_scheduler(scheduler, cityGraph):
    f = open("output.txt", "w")
    f.write(str(len(scheduler)) + "\n")
    for i,intersched in enumerate(scheduler):
        f.write(str(i) + "\n")
        z=0
        for _, time in intersched:
            if time: z+=1
        f.write(str(z) + "\n")
        for idstreet, time in intersched:  
            if time:
                f.write(cityGraph[i][idstreet] + " " + str(time) + "\n")
    f.close()

def caller(scheduler, temp, alpha, cityGraph):
    toppoints=0
    for i in range(10000):
        if i%1000==0: print(i)
        scheduler, points, best=schedule(scheduler, temp, toppoints)
        if best:
            toppoints=points
            write_scheduler(scheduler, cityGraph)
            print("New best (" + str(toppoints) + ") found and written")
        temp*=alpha
    #print (scheduler)


def main():
    scheduler, cityGraph=init_scheduler()
    write_scheduler(scheduler, cityGraph)
    caller(scheduler, temp, alpha, cityGraph)


main()
